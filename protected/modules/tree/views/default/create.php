<?php
/* @var $this DefaultController */
/* @var $model CategoryTree */

        $this->breadcrumbs=array(
            'Дерево категорий',
        );
        $this->menu=array(
            array('label'=>'Дерево категорий', 'url'=>array('index')),
            array('label'=>'Создать', 'url'=>array('create')),
        );
?>

<h1>Добавить Категорию</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'categories_list'=>CHtml::listData( $model::getTree(), 'id' , 'title'))); ?>