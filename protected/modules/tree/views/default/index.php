<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>

<div class="admin index">
    <?php
        $this->breadcrumbs=array(
            'Дерево категорий',
        );
        $this->menu=array(
            array('label'=>'Создать', 'url'=>array('create')),
        );
    ?>
    <h1>Дерево категорий</h1>
    
    <?
        $level = 0;
        echo CHtml::openTag('ul',array('class'=>'tree'))."\n";
        foreach ($categories as $i => $category) {
            if ($category->level > $level) {
                echo CHtml::openTag('ul',array('class'=>'tree__sub'))."\n";
            }
            elseif ($category->level < $level) {
                
                for ($k = 0; $k < $level - $category->level; $k++) {
                    echo CHtml::closeTag('ul')."\n";
                    echo CHtml::closeTag('li')."\n";
                }
            }
            echo CHtml::openTag('li',array('class'=>'tree__item'))."\n";
            echo $category -> title.' ';
            echo '['.CHtml::link('Изменить',"javascript:", array("submit"=>array('update', 'id'=>$category->id))).', '.CHtml::link('Удалить',"javascript:", array("submit"=>array('delete', 'id'=>$category->id), 'confirm' => 'Вы уверены?')).']';
            echo CHtml::closeTag('li')."\n";
            
            $level = $category->level;
        }
        echo CHtml::closeTag('ul');
    ?>
    
</div>