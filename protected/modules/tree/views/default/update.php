<?php
/* @var $this DefaultController */
/* @var $model CategoryTree */

$this->breadcrumbs=array(
	'Дерево категорий'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Дерево категорий', 'url'=>array('index')),
	array('label'=>'Создать', 'url'=>array('create')),
	array('label'=>'Посмотреть', 'url'=>array('view', 'id'=>$model->id))
);
?>

<h1>Редактировать Категорию "<?php echo $model->title; ?>" [<?php echo $model->id; ?>]</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'categories_list' => CHtml::listData( CategoryTree::getTreeWithoutDescendants($model), 'id' , 'title'))); ?>