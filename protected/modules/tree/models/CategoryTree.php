<?php

class CategoryTree extends Category
{
    public static function setRoot(Category  $model) {
        $model->left_key = 1;
        $model->right_key = 2;
        $model->level = 1;
        
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->insert('{{category}}', $model->attributes);
        
        return $model;
    }

    public static function getTree() {
        $categories = self::model()->findAll(array (
            'select' => array('id','title','level'),
            'order' => 'left_key ASC'
        ));
        
        return $categories;
    }
    
    public static function getDescendants(Category $root) {
        $left_key = $root -> left_key;
        $right_key = $root -> right_key;
        
        $categories = self::model()->findAll(array (
            'select' => array('id','title', 'level'),
            'condition' => 'left_key>=:left_key AND right_key<=:right_key',
            'params' => array(':left_key'=>$left_key, ':right_key' => $right_key),
            'order' => 'left_key ASC'
        ));
        
        return $categories;
    }
    
    public static function getTreeWithoutDescendants(Category $root) {
        $left_key = $root -> left_key;
        $right_key = $root -> right_key;
        
        $categories = self::model()->findAll(array (
            'select' => array('id','title', 'level'),
            'condition' => 'left_key < :left_key OR right_key > :right_key',
            'params' => array(':left_key'=>$left_key, ':right_key' => $right_key),
            'order' => 'left_key ASC'
        ));
        
        return $categories;
    }
    
    public static function getLastDescendant(Category $root) {
        $right_key = $root -> right_key;
        $level = $root -> level;
        
        $category = self::model()->find(array (
            'select' => array('id','title', 'level', 'left_key', 'right_key'),
            'condition' => 'right_key=:right_key AND level=:level',
            'params' => array(':right_key' => $right_key-1, ':level' => $level+1),
        ));
        
        if (!isset($category)) {
            $category = new Category;
            $category->right_key = $root -> left_key;
            $category->level = $root -> level + 1;
        }
        
        return $category;
    }
    
    public static function setItem(Category $model) {
        $parent_id = $model->parent_id;

        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try {
            $root = self::model()->findByPk($parent_id);
            
            if (!isset($root)) {
                self::setRoot($model);
            }
            else {
                $lastItem = self::getLastDescendant($root);
            
                // обновляем дерево       
                $command = $connection->createCommand('UPDATE {{category}} SET right_key = right_key + 2, left_key = IF(left_key > :right_key, left_key + 2, left_key) WHERE right_key > :right_key');
                $command->bindParam(":right_key", $lastItem->right_key, PDO::PARAM_STR);
                $command->execute();
                
                // добавляем новую категорию
                $model->left_key = $lastItem->right_key + 1;
                $model->right_key = $lastItem->right_key + 2;
                $model->level = $lastItem->level;
                
                $command = $connection->createCommand();
                $command->insert('{{category}}', $model->attributes);
            
            }
            
            $transaction->commit();
        }
        catch(Exception $e) {
            $transaction->rollback();
        }
        
        return $model;
    }
    
    public static function deleteItem($id) {
        
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try {
            $model = self::model()->findByPk($id);
            
            // удаляем ветку
            $command = $connection->createCommand('DELETE FROM {{category}} WHERE left_key >= :left_key AND right_key <= :right_key');
            $command->bindParam(":right_key", $model->right_key, PDO::PARAM_STR);
            $command->bindParam(":left_key", $model->left_key, PDO::PARAM_STR);
            $command->execute();
            
            // обновляем дерево       
            $command = $connection->createCommand('UPDATE {{category}} SET left_key = IF(left_key > :left_key, left_key - ( :right_key - :left_key + 1), left_key), right_key = right_key - ( :right_key - :left_key + 1) WHERE right_key > :right_key');
            $command->bindParam(":right_key", $model->right_key, PDO::PARAM_STR);
            $command->bindParam(":left_key", $model->left_key, PDO::PARAM_STR);
            $command->execute();
            
            $transaction->commit();
        }
        catch(Exception $e) {
            $transaction->rollback();
        }

        
        return false;
    }
    
    
    /**
	 * Перемещает категорию
	 * @param int $id, int $parent_id(новый родительский узел)
	 */
    public static function moveItem(Category $model) {
        $parent_id = $model->parent_id;
        
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try {
            
            $model_parent = self::model()->findByPk($parent_id);
            $withoutMove = false;
            if (!isset($model_parent)) {
                $withoutMove = true;
            }
            else {
                $model_right = self::getLastDescendant($model_parent);
                $right_key_near = $model_right->right_key;
            }
            
            if ($model->id == $parent_id) {
                $withoutMove = true;
                $model->parent_id = $model_parent->parent_id;
            }
            
/*             if (!isset($model_right)) {
                $right_key_near = $model_parent->left_key;
            } */
            
            $skew_level = $model_parent->level - $model->level + 1;
            $skew_tree = $model->right_key - $model->left_key + 1;
                
            // обновление данных
            $command = $connection->createCommand();
            $command->update('{{category}}', $model->attributes, 'id=:id', array(':id'=>$model->id));
            
            if (!$withoutMove) {
            
                // перемещение вверх
                if ($model->right_key > $right_key_near) {
                    $skew_edit = $right_key_near - $model->left_key + 1;
                    $command = $connection->createCommand('UPDATE {{category}} SET right_key = IF(left_key >= :left_key, right_key + :skew_edit, IF(right_key < :left_key, right_key + :skew_tree, right_key)), level = IF(left_key >= :left_key, level + :skew_level, level), left_key = IF(left_key >= :left_key, left_key + :skew_edit, IF(left_key > :right_key_near, left_key + :skew_tree, left_key)) WHERE right_key > :right_key_near AND left_key < :right_key');
                    $command->bindParam(":right_key", $model->right_key, PDO::PARAM_STR);
                    $command->bindParam(":left_key", $model->left_key, PDO::PARAM_STR);
                    $command->bindParam(":right_key_near", $right_key_near, PDO::PARAM_STR);
                    $command->bindParam(":skew_tree", $skew_tree, PDO::PARAM_STR);
                    $command->bindParam(":skew_level", $skew_level, PDO::PARAM_STR);
                    $command->bindParam(":skew_edit", $skew_edit, PDO::PARAM_STR);
                    $command->execute();
                }
                // перемещение вниз
                else {
                    $skew_edit = $right_key_near - $model->left_key + 1 - $skew_tree;
                    
                    $command = $connection->createCommand('UPDATE {{category}} SET left_key = IF(right_key <= :right_key, left_key + :skew_edit, IF(left_key > :right_key, left_key - :skew_tree, left_key)), level = IF(right_key <= :right_key, level + :skew_level, level), right_key = IF(right_key <= :right_key, right_key + :skew_edit, IF(right_key <= :right_key_near, right_key - :skew_tree, right_key)) WHERE right_key > :left_key AND left_key <= :right_key_near');
                    $command->bindParam(":right_key", $model->right_key, PDO::PARAM_STR);
                    $command->bindParam(":left_key", $model->left_key, PDO::PARAM_STR);
                    $command->bindParam(":right_key_near", $right_key_near, PDO::PARAM_STR);
                    $command->bindParam(":skew_tree", $skew_tree, PDO::PARAM_STR);
                    $command->bindParam(":skew_level", $skew_level, PDO::PARAM_STR);
                    $command->bindParam(":skew_edit", $skew_edit, PDO::PARAM_STR);
                    $command->execute();
                }
            }
            
            $transaction->commit();
        }
        catch(Exception $e) {
            $transaction->rollback();
        }
        
        return $model;
    }
}
