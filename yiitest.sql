-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 02 2017 г., 08:31
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yiitest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ts_category`
--

CREATE TABLE `ts_category` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `left_key` int(10) NOT NULL,
  `right_key` int(10) NOT NULL,
  `level` int(10) NOT NULL,
  `parent_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `ts_category`
--

INSERT INTO `ts_category` (`id`, `title`, `left_key`, `right_key`, `level`, `parent_id`) VALUES
(1, 'Категории', 1, 32, 1, 0),
(2, 'Категория 1', 2, 9, 2, 1),
(3, 'Категория 2', 10, 23, 2, 1),
(4, 'Категория 3', 24, 31, 2, 1),
(5, 'Категория 1.1', 3, 8, 3, 2),
(6, 'Категория 2.1', 11, 12, 3, 3),
(7, 'Категория 2.2', 13, 20, 3, 3),
(8, 'Категория 2.3', 21, 22, 3, 3),
(9, 'Категория 3.1', 25, 30, 3, 4),
(10, 'Категория 1.1.1', 4, 5, 4, 5),
(11, 'Категория 1.1.2', 6, 7, 4, 5),
(12, 'Категория 2.2.1', 14, 15, 4, 7),
(13, 'Категория 2.2.2', 16, 17, 4, 7),
(14, 'Категория 2.2.3', 18, 19, 4, 7),
(15, 'Категория 3.1.1', 26, 27, 4, 9),
(16, 'Категория 3.1.2', 28, 29, 4, 9);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ts_category`
--
ALTER TABLE `ts_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ts_category`
--
ALTER TABLE `ts_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
